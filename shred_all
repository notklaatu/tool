#!/bin/bash
#
##
###      _                  _          _ _
###  ___| |__  _ __ ___  __| |    __ _| | |
### / __| '_ \| '__/ _ \/ _` |   / _` | | |
### \__ \ | | | | |  __/ (_| |  | (_| | | |
### |___/_| |_|_|  \___|\__,_|___\__,_|_|_|
###                         |_____|
###  _ _|_ _ ._    _  _
### (_\/|_(_)|_)\/(_|(/_
###   /      |  /  _|
###
### shred_all
### (c) 2017 - 2022  |  cytopyge
###
### destroy files and directories
###
### usage: shred_all <files and/or directories>
###
##
#


# initial definitions

targets="$@"
passes=10

## initialize hardcoded variables
script_name="shred_all"
initial_release_year="2017"
developer="cytopyge"
source_dir="$XDG_DATA_HOME/c/git/code/source/function"


main()
{
	sourcing
	get_sudo
	destroying
}


sourcing()
{
	## define colors
	source $source_dir/text_appearance

	## reply functions
	source $source_dir/reply_functions

	## user authentication
	source $source_dir/get_sudo
}


destroying()
{

	#[TODO] user dialog separate
	# actual shred separate
	for target in $targets; do

		## all files in target
		file_count=$(find $target -depth -type f | wc -l)
		link_count=$(find $target -depth -type l | wc -l)
		directory_count=$(find $target -depth -type d | wc -l)

		printf "about to shred:	${YELLOW}$file_count${NOC} files, ${YELLOW}$link_count${NOC} links and ${YELLOW}$directory_count${NOC} directories in $target\n\n${RED}THIS IS DEFINITE${NOC} continue? (y/N) "

		reply_read_single_hidden_timer

		echo
		if printf "$reply" | grep -iq "^y"; then

			printf "start shredding...\n"

		elif [[ -z $reply ]]; then

			printf "${RED}timer interrupt${NOC}\n"
			exit 4

		else

			printf "${RED}user interrupt${NOC}\n"
			exit 5

		fi

		## -depth processes each directory's contents before the directory itself
		find $target -depth -type f -exec shred -n $passes --remove=wipesync -v {} \;
		## nine passes overwrite files with random data
		#shred -n 9 -v $target

		## sync disk buffers (write all cached data to filesystem)
		sync

		## final pass overwrite with zero data and remove files
		#shred -n 0 -vuz $target
		#find $target -depth -type f -exec shred -n 0 -u wipesync -v {} \;

		## nine passes overwrite directory names with zeroes
		#for (( i=1; i<=$passes; i++ ))
		#do
		##for i in {1..$total_passes}; do
		#	printf "$passes\n"
		#	counter=$(( $passes - $i ))
		#	printf "$counter\n"
		#
		#	#dir_name=$(printf "%0.s0" {1..$name} $(seq 1 $name))
		#	tmp_dir_name=$(printf "%0.s0" $(seq 1 $counter))
		#	printf "mv: $target: pass 1/$passes ($tmp_dir_name)...\n"
		#	find $target -depth -type d -exec mv -fv {} $tmp_dir_name \;
		#done

		## remove all subdirs
		[[ "$target" == "$(pwd)" ]] || sudo rm -rfv $target

		printf "${GREEN}shred_all complete${NOC}\n"

	done

}

main
