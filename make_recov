#!/usr/bin/env bash
#
##
###
### make_recov
###
### make offline recovery
###
### (c) 2022  |  cytopyge
###
### usage:
### make_recov [rsync_options]
###
##
#


# rsync options
# -a		is equivalent to -rlptgoD
# TODO --dirs?
# --dirs	will create directories specified in the list on the destination
# --links	prevent rsync error: 'skipping non-regular file'
options=$@


# initialize
repo_mount="$HOME/dock/2"
code_mount="$HOME/dock/3"
keys_mount="$HOME/dock/4"
make_offl_repo="$XDG_DATA_HOME/c/git/code/tool/make_offl_repo"


# sourcing
source_dir="$XDG_DATA_HOME/c/git/code/source/function"
source $source_dir/get_sudo
source $source_dir/reply_functions
source $source_dir/text_appearance


repo()
{
    # synchronize offline repo		(repo)

    ## dock/2 is mountpoint
    [[ $(mount | grep $repo_mount) ]] && repo_mounted=1

    case $repo_mounted in

	1)
	    $make_offl_repo "$repo_mount" "$options"
	    ;;

	*)
	    printf "\n${YELLOW}$repo_mount${NOC} not mounted\n"
	    exit 22
	    ;;

    esac
}


code()
{
    # synchronize offline code		(code)


    ## dock/3 is mountpoint
    [[ $(mount | grep $code_mount) ]] && code_mounted=1


    ## code source array (dictionary)
    declare -A code_source=( \
	[.config]="$XDG_CONFIG_HOME" \
	[code]="$XDG_DATA_HOME/c/git/code" \
        [note]="$XDG_DATA_HOME/c/git/note" \
	[prvt]="$XDG_DATA_HOME/c/git/prvt" \
    )


    case $code_mounted in

    	1)
	    printf "\n${BLUE}%s${NOC}\n" "synching offline recovery code"

	    for src_key in "${!code_source[@]}"; do

		local src=$(printf "${code_source[$src_key]}")
		local dst=$(printf "$code_mount")

		if [[ ! -d $dst ]]; then

		    printf "\n${YELLOW}$dst${NOC} not a directory\n"
		    exit 31

		fi

		rsync -aAXv \
		    --info=ALL \
		    $options \
		    $src $dst

	    done
	    ;;

    	*)
	    printf "\n${YELLOW}$code_mount${NOC} not mounted\n"
	    exit 32
	    ;;

    esac
}


keys()
{
    # synchronize keys			(keys)


    # dock/4 is mountpoint
    [[ $(mount | grep $keys_mount) ]] && keys_mounted=1


    # keys source array (dictionary)
    declare -A keys_source=( \
	[keys]="$XDG_DATA_HOME/c/keys" \
	[pass]="$(readlink $HOME/.password-store)" \
    )


    case $keys_mounted in

    	1)
	    printf "\n${BLUE}%s${NOC}\n" "synching offline recovery keys"

	    for src_key in "${!keys_source[@]}"; do

		local src=$(printf "${keys_source[$src_key]}")
		local dst=$(printf "$keys_mount")
		#local dst=$(printf "$keys_mount/$(printf "$src_key" | \
		#    awk -F / '{printf $NF}')")

		if [[ ! -d $dst ]]; then

		    printf "\n${YELLOW}$dst${NOC} not a directory\n"
		    exit 41

		fi

	        rsync -aAXv \
		    --info=ALL \
		    $options \
		    $src $dst

	    done
	    ;;

    	*)
	    printf "\n${YELLOW}$keys_mount${NOC} not mounted\n"
	    exit 42
	    ;;

    esac
}


make_recovery()
{
    repo
    code
    keys
}


main()
{
    make_recovery
}

main
